AMX-Core-Library
================

v2.0.03
----------------
- Added simple send function to Snapi API

v2.0.02
----------------
- Added support for overriding array size on params

v2.0.01 Beta
----------------
- Hotfix for SNAPI_Debug not showing when DEBUG not defined

v2.0 Beta 2
----------------
- DUET_MAX_PARAM_ARRAY_SIZE can now be overided if you want more than 10

v2.0 Beta
----------------
- Changes to DEBUG functions to reduce storage when disabled
- Debug now moved to seperate file so you can leave it out completely if needed
- New Function: NumberArrayCount()
- New Function: NumberFind()

v1.07
----------------
- Now out of Beta
- SNAPI processing improvements. Added 'SNAPI_InitDataFromDevice()' function to include device addressing in the _SNAPI data type

v1.06
----------------

- Debugging for SNAPI now reports the device
- DebugAddNumberToArray() function added to add integer values in debugging
- SNAPI_InitParams() for initialising param arrays for sending SNAPI type commands

v1.05
----------------
- SNAPI_Debug() function to send the SNAPI processing to the console
